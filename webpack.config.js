const path = require('path');
const webpack = require('webpack');

const paths = {
	app: path.join(__dirname, 'src'),
	build: path.join(__dirname, 'build')
};

module.exports = {
	devtool: 'eval-source-map',
	entry: paths.app,
	target: 'node',

	output: {
		path: paths.build,
		filename: 'build.js'
	},

	plugins: [
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.UglifyJsPlugin()
	],

	module: {
		loaders: [
			{
				test: /\.js$/,
				loaders: ['babel'],
				exclude: /node_modules/,
				inlcude: __dirname
			}
		]
	}
};
